"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removePost = exports.updatePost = exports.createPost = exports.getPost = exports.removePosts = exports.updatePosts = exports.sendPosts = exports.getPosts = void 0;
/**
 * GET POST UPDATE(not supported) REMOVE
 */
exports.getPosts = (req, res) => {
    res.status(200).json({ success: true, msg: "GET all posts" });
};
exports.sendPosts = (req, res) => {
    res.status(201).json({ success: true, msg: "created posts" });
};
exports.updatePosts = (req, res) => {
    res.status(402).json({ success: false, msg: "Put operation not supported" });
};
exports.removePosts = (req, res) => {
    res.status(402).json({ success: true, msg: "removed all" });
};
/**
 * GET UPDATE DELETE BY ID
 */
exports.getPost = (req, res) => {
    res.status(200).json(`get post number : ${req.params.id}`);
};
exports.createPost = (req, res) => {
    res.status(200).json(`post operation not supported`);
};
exports.updatePost = (req, res) => {
    res.status(200).json({ success: true, msg: `updated number : ${req.params.id}` });
};
exports.removePost = (req, res) => {
    res.status(200).json({ success: true, msg: `removed number : ${req.params.id}` });
};
//# sourceMappingURL=postControllers.js.map