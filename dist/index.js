"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const dotenv = __importStar(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const postRouter_1 = require("./routes/postRouter");
const homeRouter_1 = require("./routes/homeRouter");
const error_middleware_1 = require("./middleware/error.middleware");
const notFound_middleware_1 = require("./middleware/notFound.middleware");
// initial configuration dotenv
dotenv.config();
/*
 * Configuration App
 */
const app = express_1.default();
const server = new http_1.default.Server(app);
app.use(helmet_1.default());
app.use(cors_1.default());
app.use(express_1.default.json());
//routers
app.use("/", homeRouter_1.homeRouter);
app.use("/posts", postRouter_1.postRouter);
//Error handle  
app.use(error_middleware_1.errorHandler);
app.use(notFound_middleware_1.notFoundHandler);
// server and port
const port = process.env.PORT || 8080;
server.listen(port, () => {
    console.log(`server started at port ${port}`);
});
//# sourceMappingURL=index.js.map