"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postRouter = void 0;
const express_1 = __importDefault(require("express"));
const postControllers_1 = require("../controllers/postControllers");
exports.postRouter = express_1.default.Router();
//get all posts
exports.postRouter.route("/")
    .get(postControllers_1.getPosts)
    .post(postControllers_1.sendPosts)
    .put(postControllers_1.updatePosts)
    .delete(postControllers_1.removePosts);
//get by id
exports.postRouter.route("/:id")
    .get(postControllers_1.getPost)
    .post(postControllers_1.createPost)
    .put(postControllers_1.updatePost)
    .delete(postControllers_1.removePost);
//# sourceMappingURL=postRouter.js.map