"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.homeRouter = void 0;
const express_1 = __importDefault(require("express"));
exports.homeRouter = express_1.default.Router();
exports.homeRouter.route("/")
    .get((req, res) => {
    res.status(200).json({
        success: true,
        msg: "Hello World!"
    });
});
//# sourceMappingURL=homeRouter.js.map