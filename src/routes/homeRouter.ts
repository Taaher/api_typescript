import express from 'express'
import {Request,Response} from 'express'
export const homeRouter = express.Router()

homeRouter.route("/")
    .get((req: Request, res: Response) => {
        res.status(200).json({
            success: true,
            msg:"Hello World!"
        })
    })