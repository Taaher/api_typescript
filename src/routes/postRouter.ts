import express from 'express'
import {
    getPosts,
    sendPosts,
    updatePosts,
    removePosts,
    //with id
    getPost,
    createPost,
    updatePost,
    removePost
} from '../controllers/postControllers'
export const postRouter = express.Router()

//get all posts
postRouter.route("/")
    .get(getPosts)
    .post(sendPosts)
    .put(updatePosts)
    .delete(removePosts)
//get by id
postRouter.route("/:id")
    .get(getPost)
    .post(createPost)
    .put(updatePost)
    .delete(removePost)



