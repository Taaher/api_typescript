import express from "express"
import http from 'http'
import * as dotenv from "dotenv"
import cors from "cors"
import helmet from "helmet"
import { postRouter } from './routes/postRouter'
import { homeRouter } from './routes/homeRouter'
import { errorHandler } from "./middleware/error.middleware"
import {notFoundHandler} from "./middleware/notFound.middleware"

// initial configuration dotenv
dotenv.config()

/*
 * Configuration App
 */
const app = express()
const server = new http.Server(app)

app.use(helmet());
app.use(cors());
app.use(express.json());


//routers
app.use("/", homeRouter)
app.use("/posts", postRouter)

//Error handle  
app.use(errorHandler);
app.use(notFoundHandler);

// server and port
const port = process.env.PORT || 8080
server.listen(port,() => {
    console.log(`server started at port ${port}`)
})