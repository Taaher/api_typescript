import express, { Request, Response } from "express"


/**
 * GET POST UPDATE(not supported) REMOVE
 */
export const getPosts = (req:Request,res:Response) => {
    res.status(200).json({ success:true,msg:"GET all posts" })
}
export const sendPosts = (req:Request,res:Response) => {
    res.status(201).json({success:true,msg:"created posts"})
}
export const updatePosts = (req:Request,res:Response) => {
    res.status(402).json({success:false ,msg: "Put operation not supported"})
}
export const removePosts = (req:Request,res:Response) => {
    res.status(402).json({success:true ,msg: "removed all"})
}

/**
 * GET UPDATE DELETE BY ID 
 */

export const getPost = (req:Request,res:Response) => {
    res.status(200).json(`get post number : ${req.params.id}`)
}
export const createPost = (req:Request,res:Response) => {
    res.status(200).json(`post operation not supported`)
}

export const updatePost = (req:Request,res:Response) => {
    res.status(200).json({success:true,msg:`updated number : ${req.params.id}`})
}
export const removePost = (req:Request,res:Response) => {
    res.status(200).json({success:true,msg:`removed number : ${req.params.id}`})
}